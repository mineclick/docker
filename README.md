# Docker base image

This docker image is used by most projects to simplify deployment.

## Local development

1. Clone and install (`mvn install`) the following projects, in the order listed:
    - [Messenger](https://gitlab.com/mineclick/messenger)
    - [Global](https://gitlab.com/mineclick/global)
    - [Game](https://gitlab.com/mineclick/game)
    - [MCBungee](https://gitlab.com/mineclick/bungee)
    - [Ender](https://gitlab.com/mineclick/ender)
2. Run `docker-compose -f docker-compose.dev.yml up` to start the Redis and MongoDB containers.
3. Import the `mineclick.settings.json` MongoDB collection found in
   the [Config repo](https://gitlab.com/mineclick/config/-/blob/master/mineclick.settings.json). You can
   use [MongoDB Compass](https://www.mongodb.com/products/compass) to do this. This needs to be saved under
   the `mineclick` database in the `settings` collection.
4. Run the following commands in separate terminals:
    - In the `folders/ender` folder, run `./test.sh` to run Ender (port 8088 for web; 8089 for api)
    - In the `folders/bungee` folder, run `./test.sh` to run Bungee (port 25566)
    - In the `folders/game` folder, run `./test.sh <path to Config root dir>` to run the Game. For example, if the
      Config repo is in the same directory as the Game repo, you would run `./test.sh ../config`
5. You can now connect to the server using the IP `localhost` and the port `25566`.
6. Join and leave the server - this will create a new player record in your MongoDB instance. Now find your player's MongoDB document in the `mineclick.players` collection and set your rank to `DEV`. Once again use
   MongoDB Compass to do this.

If you have any issues, please write in the `#dev` channel on Discord.