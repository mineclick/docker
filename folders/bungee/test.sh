#!/usr/bin/env bash

# make a temporary folder
folder=../../temp/bungee_$(head /dev/urandom | tr -dc a-z0-9 | head -c 5)
mkdir -p "$folder"
cp -R . "$folder"/
cd "$folder" || (echo "No temp folder" && exit)

cp ~/.m2/repository/net/mineclick/bungee/MCBungee/master/MCBungee-master.jar ./plugins/MCBungee.jar

java -Xms512M -Xmx1G -jar BungeeCord.jar --noconsole

# cleanup
rm -rf "$folder"