#!/usr/bin/env bash
. ../common/packageUtil.sh
. ../common/gitUtil.sh

# Download Game, Global, Libraries and move to plugins
download net/mineclick/game Game "$BRANCH" Game.jar || exit 1
mv Game.jar ./plugins
download net/mineclick/core Global "$BRANCH" Global.jar || exit 1
mv Global.jar ./plugins

# Extract world/region.tar.xz
# To archive: tar -czvf region.tar.gz region
tar -C ./world/ -xzvf ./world/region.tar.gz

# Download configurations
clone config "$BRANCH"

# Run
exec java -Xms2G -Xmx6G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -jar paper.jar --noconsole nogui
