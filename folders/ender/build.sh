#!/usr/bin/env bash
. ../common/packageUtil.sh

# Download Ender
download net/mineclick/ender Ender "$BRANCH" Ender.jar

# Run
java -jar Ender.jar
