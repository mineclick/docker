#!/usr/bin/env bash
. ../common/packageUtil.sh

# Download DockerHealth
download net/mineclick/docker/health DockerHealth "master" DockerHealth.jar

# Run
java -jar DockerHealth.jar
