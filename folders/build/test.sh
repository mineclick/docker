#!/usr/bin/env bash
. ../common/gitUtil.sh

# make a temporary folder
folder=../../temp/build_$(head /dev/urandom | tr -dc a-z0-9 | head -c 5)
mkdir -p "$folder"
cp -R . "$folder"/
cd "$folder" || (echo "No temp folder" && exit)

# Extract world/region.tar.xz
tar -C ./world/ -xzvf ./world/region.tar.gz
tar -C ./build/ -xzvf ./build/region.tar.gz

# Download configurations
clone config staging

# Copy the plugins from local maven folder
cp ~/.m2/repository/net/mineclick/build/Build/master/Build-master.jar ./plugins/Build.jar
cp ~/.m2/repository/net/mineclick/core/Global/masterT/Global-master.jar ./plugins/Global.jar

# run
#export MC_DEBUG=true
java -Xms2G -Xmx4G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -jar paper.jar --noconsole nogui
#java -Xms1G -Xmx2G -XX:+UseConcMarkSweepGC -jar spigot.jar --noconsole nogui

# cleanup
rm -rf "$folder"
