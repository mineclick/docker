#!/usr/bin/env bash

# Test the arguments are passed
if test -z "$FOLDER"; then echo "Folder undefined!" && sleep 5 && exit 1; fi

# Check the build script exists in the folder
cd "$FOLDER" || (echo "Error, no such directory $FOLDER" && sleep 5 && exit 1)
if test -z build.sh; then echo "Build script is not found!" && sleep 5 && exit 1; fi

# Run it!
chmod +x ./build.sh && exec ./build.sh
