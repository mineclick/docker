FROM openjdk:17-slim
STOPSIGNAL SIGINT
WORKDIR /app
COPY entryPoint.sh folders ./
ARG NUVOTIFIER_PRIVATE_KEY
ARG NUVOTIFIER_PUBLIC_KEY
ARG GITLAB_TOKEN
ARG SENTRY_URL
ENV GITLAB_TOKEN=$GITLAB_TOKEN
ENV SENTRY_URL=$SENTRY_URL
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN apt-get update && \
  apt-get install -y git wget fontconfig fonts-dejavu libfreetype6 \
  && rm -rf /var/lib/apt/lists/* \
  && chmod +x /tini \
  && chmod +x entryPoint.sh \
  && mkdir bungee/plugins/NuVotifier/rsa \
  && echo ${NUVOTIFIER_PRIVATE_KEY} >> bungee/plugins/NuVotifier/rsa/private.key \
  && echo ${NUVOTIFIER_PUBLIC_KEY} >> bungee/plugins/NuVotifier/rsa/public.key
ENTRYPOINT ["/tini", "-v", "--", "./entryPoint.sh"]